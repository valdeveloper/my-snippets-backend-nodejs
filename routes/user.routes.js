const router = require("express").Router()
const authController = require("../controllers/auth.controller")
const userController = require("../controllers/user.controller")
const noteController = require("../controllers/note.controller")
const authMiddleware = require("../middleware/auth.middleware")

router.post("/register", authController.signUp)
router.post("/login", authController.signIn)
router.get("/logout", authController.logout)

// User db
router.get("/:id", authMiddleware.privateAccess, userController.userInfo)
router.delete("/:id", authMiddleware.privateAccess, userController.deleteUser)

// CRUD Notes of user
router.post("/:id/note", authMiddleware.privateAccess, noteController.insertNote) // Insert
router.get("/:id/note", authMiddleware.privateAccess, noteController.getAllNotes) // get all
router.get("/:id/note/:noteId", authMiddleware.privateAccess, noteController.getNoteById) // get one
router.put("/:id/note/:noteId", authMiddleware.privateAccess, noteController.updateNote) // update
router.delete("/:id/note/:noteId", authMiddleware.privateAccess, noteController.deleteNote) // delete

module.exports = router
