const express = require("express")
const cors = require("cors")
const cookieParser = require('cookie-parser')

require("dotenv").config({ path: './config/.env' })
require("./config/db")

const userRoutes = require("./routes/user.routes")
const { checkUser, requireAuth } = require("./middleware/auth.middleware")

const app = express() 

const corsOptions = {
    origin: process.env.CLIENT_URL,
    credentials: true,
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
} 

app.use(cors(corsOptions))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cookieParser())

// JWT
app.get('*', checkUser)
app.get('/jwtid', requireAuth, (req, res) => {
    res.status(200).send(res.locals.user._id) // ??
})

app.use('/api/v1/user', userRoutes)

const port =
    process.env.NODE_ENV === "production" ? process.env.PORT || 80 : 4000
    
app.listen(port, () => {
    console.log(`MySnippet app listening at http://localhost:${port}`)
})
