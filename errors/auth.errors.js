module.exports.signUpErrors = (err) => {
    let errors = { username: '', email: '', password: '', passwordConfirm: '' }

    console.log(err.message)

    if (err.message.includes('username')) {
        errors.username = "Nom d'utilisateur incorrect ou déjà pris"
    }

    if (err.message.includes('email')) {
        errors.email = "Email incorrect"
    }

    if (err.message.includes('password') && err.message.includes('shorter')) {
        errors.password = "Le mot de passe doit faire minimum 6 caractères"
    }

    if (err.message.includes('passwordConfirm')) {
        errors.passwordConfirm = "La confirmation du mot de passe est incorrecte"
    }

    if (err.code === 11000 && Object.keys(err.keyValue)[0].includes('email')) {
        errors.email = "Cet email existe déjà"
    }

    if (err.code === 11000 && Object.keys(err.keyValue)[0].includes('pseudo')) {
        errors.pseudo = "Ce pseudo est déjà pris"
    }

    return errors
}

module.exports.signInErrors = (err) => {
    let errors = { email: '', password: '' }

    console.log(err.message)

    if (err.message.includes("email")) errors.email = "Email inconnu"

    if (err.message.includes("password")) errors.password = "Le mot de passe est incorrect"

    return errors
}