const UserModel = require('../models/user.model')
const jwt = require('jsonwebtoken')
const { signUpErrors, signInErrors } = require('../errors/auth.errors')

const createToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRES_IN })
}

module.exports.signUp = async (req, res) => {

    console.log(req.body)

    const { username, email, password, passwordConfirm } = req.body

    try {
        const user = await UserModel.create({ username, email, password, passwordConfirm })
        res.status(201).json({ user: user._id })
    } catch (err) {
        const errors = signUpErrors(err)
        res.status(200).send({ errors })
    }
}

module.exports.signIn = async (req, res) => {
    const { email, password } = req.body

    try {
        const user = await UserModel.login(email, password)
        const token = createToken(user._id)

        res.cookie('jwt', token, {
            maxAge: process.env.JWT_EXPIRES_IN,
            httpOnly: true,
            secure: req.secure || req.headers["x-forwarded-proto"] === "https"
        })

        // Remove password from output for security
        // user.password = undefined

        res.status(200).json({ user: user._id })
    } catch (err) {
        const errors = signInErrors(err)
        res.status(200).json({ errors })
    }
}

module.exports.logout = (req, res) => {
    res.cookie('jwt', 'byebye', { 
        maxAge: 1,
        httpOnly: true
    })
    res.status(200).json({message : "logout"})
    //res.redirect('/')
}