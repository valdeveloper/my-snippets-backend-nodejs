const UserModel = require("../models/user.model")
const ObjectID = require("mongoose").Types.ObjectId
const { insertNoteErrors } = require("../errors/note.errors")

module.exports.insertNote = (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("ID unknown : " + req.params.id)
    }

    const { title, content } = req.body

    UserModel.findByIdAndUpdate(
        req.params.id,
        {
            $push: {
                notes: {
                    title: title,
                    content: content,
                },
            },
        },
        { new: true, runValidators: true },
        (err, docs) => {
            if (err) {
                const errors = insertNoteErrors(err)
                return res.status(200).send({ errors })
            } else {
                return res.status(201).send(docs.notes[docs.notes.length -1])
            }
        }
    )
}

module.exports.getAllNotes = (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("ID unknown : " + req.params.id)
    }

    UserModel.findById(req.params.id, (err, docs) => {
        if (!err) {
            res.status(200).send(docs.notes)
        } else {
            console.log("ID unknown : " + err)
            res.status(200).send({ message: "An error occured" })
        }
    }).select("notes")
}

module.exports.getNoteById = (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("ID unknown : " + req.params.id)
    }

    if (!ObjectID.isValid(req.params.noteId)) {
        return res.status(400).send("ID unknown : " + req.params.noteId)
    }

    UserModel.findById(req.params.id, (err, docs) => {
        if (!err) {
            const note = docs.notes.find(
                (note) => req.params.noteId === note._id.toString()
            )
            if (note) {
                res.status(200).send(note)
            } else {
                res.status(200).send({ message: "Note not found" })
            }
        } else {
            console.log("ID unknown : " + err)
            res.status(200).send({ message: "An error occured" })
        }
    }).select("notes")
}

module.exports.updateNote = async (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("ID unknown : " + req.params.id)
    }

    if (!ObjectID.isValid(req.params.noteId)) {
        return res.status(400).send("ID unknown : " + req.params.noteId)
    }

    UserModel.updateOne(
        {
            _id: req.params.id,
            notes: { $elemMatch: { _id: req.params.noteId } },
        },
        {
            $set: {
                "notes.$.title": req.body.title,
                "notes.$.content": req.body.content,
            },
        },
        { runValidators: true },
        (err, docs) => {
            if (err) console.log(err)
            res.status(200).send({ message: "Note updated successfully" })
        }
    )
}

module.exports.deleteNote = async (req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("ID unknown : " + req.params.id)
    }

    if (!ObjectID.isValid(req.params.noteId)) {
        return res.status(400).send("ID unknown : " + req.params.noteId)
    }

    UserModel.findById(req.params.id, (err, docs) => {
        if (!err) {
            const note = docs.notes.find(
                (note) => req.params.noteId === note._id.toString()
            )
            if (!note) {
                res.status(200).send({ message: "Note not found" })
            } else {
                UserModel.findByIdAndUpdate(
                    req.params.id,
                    {
                        $pull: {
                            notes: {
                                _id: req.params.noteId,
                            },
                        },
                    },
                    { new: true },
                    (err, docs) => {
                        if (!err)
                            return res.send({
                                message: "Note deleted successfully",
                            })
                        else return res.status(400).send(err)
                    }
                )
            }
        } else {
            console.log("ID unknown : " + err)
            res.status(200).send({ message: "An error occured" })
        }
    }).select("notes")
}
